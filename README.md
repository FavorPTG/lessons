<p><h1>Репозиторий домашних заданий</h1></p><br>

<p><h3>Второй модуль:</h3></p>

    Домашнее задание 1 - Program for calculating finances
    Домашнее задание 2 - Accumulation calculation
    Дополнительное задание урока 15 - Solving quadratic equations
    Домашнее задание 3 - Ticket price calculation
    Домашнее задание 4 - Ticket type detection
    Домашнее задание 5 - Fule cost calculation
    Домашнее задание 6 - Timer

<p><h3>Третий модуль:</h3></p>

    Домашнее задание 1 - Counter with data storage
    Домашнее задание 2 - Shreodinger's cat
    Домашнее задание 3 - Music player
    Домашнее задание 4 - Cool player
    Домашнее задание 5 - Cool player 2

<p><h3>Четвёртый модуль:</h3></p>

    Домашнее задание 1 - Cool animation
    Домашнее задание 2 - Train ticket
    Домашнее задание 4 - Notebook
