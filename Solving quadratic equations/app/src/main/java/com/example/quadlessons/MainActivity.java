package com.example.quadlessons;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.EOFException;

public class MainActivity extends AppCompatActivity {

    TextView a, b, c, res;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        a = findViewById(R.id.a);
        b = findViewById(R.id.b);
        c = findViewById(R.id.c);
        res = findViewById(R.id.textView2);
        btn = findViewById(R.id.button);
        Algoritm algoritm = new Algoritm();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    int ai = Integer.parseInt(a.getText().toString());
                    int bi = Integer.parseInt(b.getText().toString());
                    int ci = Integer.parseInt(c.getText().toString());

                    res.setText(algoritm.countRoots(ai, bi, ci));
                } catch (Exception error){
                    res.setText("Ошибка");
                }
            }
        });
    }
}