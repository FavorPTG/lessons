package com.example.quadlessons;

public class Algoritm {

    public String countRoots(int a, int b, int c){
        double d = (b ^ 2) - 4 * a * c;

        if(d < 0)
            return "Нет корней";
        else if(d == 0){
            double x1 = -b / (2 * a);
            return "x1 = x2 = " + x1;
        }
        else{
            double x1 = (-b + Math.sqrt(d)) / (2 * a);
            double x2 = (-b - Math.sqrt(d)) / (2 * a);
            return "x1 = " + x1 + "\nx2 = " + x2;
        }

    }
}
