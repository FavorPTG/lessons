package com.example.timer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button buttonStart;
    private Button buttonPause;
    private Button buttonStop;
    private Button buttonTimer;
    private TextView textView;

    private long startTime = 0L;
    private long timeInMillySeconds = 0L;
    private long pauseTime = 0L;
    private long updateTime = 0L;

    private Handler handler = new Handler(); // Новый поток

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        buttonStart = findViewById(R.id.startButton);
        buttonPause = findViewById(R.id.pauseButton);
        buttonStop = findViewById(R.id.stopButton);
        buttonTimer = findViewById(R.id.timerButton);

        buttonStart.setOnClickListener(listener);
        buttonPause.setOnClickListener(listener);
        buttonStop.setOnClickListener(listener);
        buttonTimer.setOnClickListener(listener);
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.startButton:
                    startTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);
                    break;
                case R.id.pauseButton:
                    pauseTime += timeInMillySeconds;
                    handler.removeCallbacks(runnable);
                    break;
                case R.id.stopButton:
                    //System.out.println("\n\t\t\t" + startTime + "\n");
                    //System.out.println("\n\t\t\t" + timeInMillySeconds + "\n");
                    startTime = 0L;
                    pauseTime = 0L;
                    handler.removeCallbacks(runnable);
                    handler.removeCallbacks(timer);
                    textView.setText("00:0:00.0");
                    break;
                case R.id.timerButton:
                    startTime = SystemClock.uptimeMillis();
                    pauseTime += timeInMillySeconds;
                    handler.removeCallbacks(runnable);
                    handler.postDelayed(timer, 0);
                    break;
            }
        }
    };

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            timeInMillySeconds = SystemClock.uptimeMillis() - startTime;
            updateTime = pauseTime + timeInMillySeconds;

            int milliseconds = (int) updateTime % 1000;
            int seconds = (int) updateTime / 1000;
            int minutes = seconds / 60;
            int hours = minutes / 60;
            int days = hours / 24;

            seconds %= 60;
            minutes %= 60;
            hours %= 24;

            textView.setText(days + "" + hours + ":" + minutes + ":" + String.format("%02d", seconds) + "." + String.format("%03d", milliseconds));
            handler.postDelayed(this, 0);
        }
    };

    private Runnable timer = new Runnable() {
        @Override
        public void run() {
            timeInMillySeconds = SystemClock.uptimeMillis() - startTime;
            updateTime = pauseTime - timeInMillySeconds;

            int milliseconds = (int) updateTime % 1000;
            int seconds = (int) updateTime / 1000;
            int minutes = seconds / 60;
            int hours = minutes / 60;
            int days = hours / 24;

            seconds %= 60;
            minutes %= 60;
            hours %= 24;

            textView.setText(days + "" + hours + ":" + minutes + ":" + String.format("%02d", seconds) + "." + String.format("%03d", milliseconds));
            handler.postDelayed(this, 0);

            if(updateTime <= 0) {
                handler.removeCallbacks(timer);
                textView.setText("00:0:00.0");
            }
        }
    };
}