package com.example.tickettypedetection;

import android.widget.TextView;
import android.widget.Toast;

import java.sql.Array;

public class Algoritm {

    private MainActivity mainActivity;

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public boolean getResult(int ticketNumber){
        int[] numbers = {0, 0, 0, 0, 0, 0};
        if((ticketNumber < 1000000) && (ticketNumber > 99999)){

            for(int i=5; i>=0; i--){
                numbers[i] = ticketNumber % 10;
                ticketNumber /= 10;
                System.out.println(i+") "+numbers[i]);

                /*
                 * numbers[0] = 123123 & 10 = 3
                 * numbers[1] = 12312 & 10 = 2
                 * numbers[2] = 1231 & 10 = 1
                 * numbers[3] = 123 & 10 = 3
                 */
            }

            int n1 = numbers[0] + numbers[2] + numbers[4];
            int n2 = numbers[1] + numbers[3] + numbers[5];

            if(n1 == n2)
                return true;
            else
                return false;

        } else if(ticketNumber >= 1000000){
            Toast.makeText(mainActivity, "Слишком длинное число", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            Toast.makeText(mainActivity, "Слишком короткое число", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
