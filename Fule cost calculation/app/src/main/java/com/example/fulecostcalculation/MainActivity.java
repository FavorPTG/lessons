package com.example.fulecostcalculation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView result;
    EditText textMass;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result = findViewById(R.id.result);
        textMass = findViewById(R.id.objectMass);
        btn = findViewById(R.id.button);

        Algoritm algoritm = new Algoritm();
        algoritm.mainActivity = this;

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    float mass = Float.parseFloat(textMass.getText().toString());
                    result.setText(String.valueOf(algoritm.fuleCalculate(mass)) + " литров");
                } catch (Exception e){
                    Toast.makeText(MainActivity.this, "Пустая строка", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}