package com.example.musicplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    private String nameAudio = "";
    private final String link = "https://muznow.net/uploads/music/2023/02/Geoffplaysguitar_Zemlyane_Atomic_Heart_Trava_u_Doma.mp3";

    private MediaPlayer mp;
    private AudioManager audioManager;

    private TextView nameTrack;
    private Switch loop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        nameTrack = findViewById(R.id.textOut);
        loop = findViewById(R.id.switchLoop);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        loop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(mp != null) {
                    mp.setLooping(isChecked);
                    nameTrack.setText(nameAudio + "\n(проигрывание " + mp.isPlaying() + ", время " + mp.getCurrentPosition()
                            + ",\nповтор " + mp.isLooping() + ", громкость " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + ")");
                }
            }
        });
    }

    public void onClickSource(View view) throws IOException {

        releaseMediaPlayer();

        try {
            switch (view.getId()) {
                case R.id.btnStream :
                    Toast.makeText(this, "Запущен поток аудио", Toast.LENGTH_SHORT).show();

                    mp = new MediaPlayer();
                    mp.setDataSource(link);
                    mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mp.setOnPreparedListener(this);
                    mp.prepareAsync();
                    nameAudio = "Песня из Atomic heart";
                    nameTrack.setText(nameAudio + "\n(проигрывание " + mp.isPlaying() + ", время " + mp.getCurrentPosition()
                            + ",\nповтор " + mp.isLooping() + ", громкость " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + ")");

                    break;
                case R.id.btnRAW :
                    Toast.makeText(this, "Запущен аудио-файл с памяти телефона", Toast.LENGTH_SHORT).show();

                    mp = MediaPlayer.create(this, R.raw.gotika);
                    mp.start();

                    nameAudio = "Музыка из Готики";
                    nameTrack.setText(nameAudio + "\n(проигрывание " + mp.isPlaying() + ", время " + mp.getCurrentPosition()
                            + ",\nповтор " + mp.isLooping() + ", громкость " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + ")");

                    break;

            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Источник информации не найден", Toast.LENGTH_SHORT).show();
        }

        if (mp == null) return;

        mp.setLooping(loop.isChecked()); // включение / выключение повтора
        mp.setOnCompletionListener(this); // слушатель окончания проигрывания
    }

    public void onClick(View view) {
        if (mp == null) return;
        switch (view.getId()) {
            case R.id.btnResume:
                if (!mp.isPlaying()) {
                    mp.start(); // метод возобновления проигрывания
                }
                break;
            case R.id.btnPause:
                if (mp.isPlaying()) {
                    mp.pause(); // метод паузы
                }
                break;
            case R.id.btnStop:
                mp.stop(); // метод остановки
                break;
            case R.id.btnForward:
                mp.seekTo(mp.getCurrentPosition() + 5000); // переход к определённой позиции трека
                // mediaPlayer.getCurrentPosition() - метод получения текущей позиции
                break;
            case R.id.btnBack:
                mp.seekTo(mp.getCurrentPosition() - 5000); // переход к определённой позиции трека
                break;
        }
        // информативный вывод информации
        nameTrack.setText(nameAudio + "\n(проигрывание " + mp.isPlaying() + ", время " + mp.getCurrentPosition()
                + ",\nповтор " + mp.isLooping() + ", громкость " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + ")");
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        Toast.makeText(this, "Старт медиа-плейера", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        Toast.makeText(this, "Отключение медиа-плейера", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseMediaPlayer();
    }

    private void releaseMediaPlayer() {
        if (mp != null) {
            mp.release();
            mp = null;
        }
    }
}